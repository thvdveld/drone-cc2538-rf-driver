use drone_cc2538_dma::DmaChannel;

pub(crate) const IEEE802154_DEFAULT_CHANNEL: u32 = 26;

pub(crate) const CC2538_RF_CCA_THRES_USER_GUIDE: u32 = 0xF8;
pub(crate) const CC2538_RF_TX_POWER_RECOMMENDED: u32 = 0xD5;
pub(crate) const CC2538_RF_CHANNEL_MIN: u32 = 11;
pub(crate) const CC2538_RF_CHANNEL_MAX: u32 = 26;
pub(crate) const CC2538_RF_CHANNEL_SPACING: u32 = 5;
pub(crate) const CC2538_RF_MAX_PACKET_LEN: u32 = 127;
pub(crate) const CC2538_RF_MIN_PACKET_LEN: u32 = 4;
pub(crate) const CC2538_RF_CCA_CLEAR: u32 = 1;
pub(crate) const CC2538_RF_CCA_BUSY: u32 = 0;

pub(crate) const RSSI_OFFSET: i32 = 73;
pub(crate) const RSSI_INVALID: i32 = -128;

pub(crate) const OUTPUT_POWER: [(i32, u8); 14] = [
    (7, 0xff),
    (5, 0xed),
    (3, 0xd5),
    (1, 0xc5),
    (0, 0xb6),
    (-1, 0xB0),
    (-3, 0xA1),
    (-5, 0x91),
    (-7, 0x88),
    (-9, 0x72),
    (-11, 0x62),
    (-13, 0x58),
    (-15, 0x42),
    (-24, 0x00),
];

pub(crate) const OUTPUT_POWER_MAX: i32 = 7;
pub(crate) const OUTPUT_POWER_MIN: i32 = -24;

#[derive(PartialEq)]
pub(crate) enum RADIO_POWER_MODE {
    OFF,
    ON,
    CARRIER_ON,
    CARRIER_OFF,
}

pub(crate) const CHECKSUM_LEN: u32 = 2;
pub(crate) const MAX_PAYLOAD_LEN: u32 = CC2538_RF_MAX_PACKET_LEN - CHECKSUM_LEN;
pub(crate) const CRC_BIT_MASK: u8 = 0x80;

pub(crate) const CC2538_RF_CONF_TX_USE_DMA: bool = true;
pub(crate) const CC2538_RF_CONF_TX_DMA_CHAN: DmaChannel = DmaChannel::Channel2;
pub(crate) const DMA_TX_FLAGS: u32 = 0; // XXX: check this flag
