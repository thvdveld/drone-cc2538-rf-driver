#![no_std]
#![allow(dead_code)]
#![feature(never_type)]

// TODO:
// - find out how to create a process in Drone OS
// - rustify the driver
// - add MAC timer

use core::convert::TryInto;

use drone_core::prelude::Box;
use drone_cortexm::fib;

use drone_cortexm::reg::prelude::*;
use drone_tisl_map::periph::radio::RadioPeriph;
use drone_tisl_map::periph::radio::RfCore;

use drone_cc2538_dma::DmaDriver;

mod consts;
use consts::*;

#[derive(Default)]
struct RfStatus {
    rx_active: bool,
}

impl RfStatus {
    fn set_rx_active(&mut self) {
        self.rx_active = true;
    }

    fn clear_rx_active(&mut self) {
        self.rx_active = false;
    }
}

struct RfConfiguration {
    rf_channel: u32,
    poll_mode: bool,
    send_on_cca: bool,
}

impl Default for RfConfiguration {
    fn default() -> Self {
        Self {
            rf_channel: IEEE802154_DEFAULT_CHANNEL,
            poll_mode: false,
            send_on_cca: true,
        }
    }
}

struct RfDriver<'dma> {
    rfcore: RadioPeriph<RfCore>,
    dma_driver: &'dma mut DmaDriver,
    rf_status: RfStatus,
    config: RfConfiguration,
}

enum RfCspOp {
    ISRXON = 0xE3,
    ISTXON = 0xE9,
    ISTXONCCA = 0xEA,
    ISRFOFF = 0xEF,
    ISFLUSHRX = 0xED,
    ISFLUSHTX = 0xEE,
}

impl<'dma> RfDriver<'dma> {
    pub fn new(rfcore: RadioPeriph<RfCore>, dma_driver: &'dma mut DmaDriver) -> Self {
        Self {
            rfcore,
            dma_driver,
            rf_status: Default::default(), // TODO: check this value.
            config: Default::default(),
        }
    }

    /// Returns a value in [11,26] representing the current channel.
    pub fn get_channel(&self) -> u32 {
        self.config.rf_channel
    }

    /// Set the current operating channel [11,26]
    pub fn set_channel(&mut self, channel: u32) {
        assert!(channel >= 11);
        assert!(channel <= 26);

        let was_on = if self
            .rfcore
            .rfcore_xreg_fsmstat0
            .fsm_ffctrl_state
            .read_bits()
            != 0
        {
            self.off();
            true
        } else {
            false
        };

        self.rfcore.rfcore_xreg_freqctrl.store(|r| {
            r.write_freq(
                CC2538_RF_CHANNEL_MIN
                    + (channel - CC2538_RF_CHANNEL_MIN) * CC2538_RF_CHANNEL_SPACING,
            )
        });

        if was_on {
            self.on();
        }

        self.config.rf_channel = channel;
    }

    pub fn get_pan_id(&self) -> u32 {
        (self.rfcore.rfcore_ffsm_pan_id1.load_bits() << 8)
            | self.rfcore.rfcore_ffsm_pan_id0.load_bits()
    }

    pub fn set_pan_id(&mut self, pan_id: u32) {
        self.rfcore.rfcore_ffsm_pan_id0.store_bits(pan_id & 0xff);
        self.rfcore.rfcore_ffsm_pan_id1.store_bits(pan_id >> 8);
    }

    pub fn get_short_addr(&self) -> u32 {
        (self.rfcore.rfcore_ffsm_short_addr1.load_bits() << 8)
            | self.rfcore.rfcore_ffsm_short_addr0.load_bits()
    }

    pub fn set_short_addr(&mut self, addr: u32) {
        self.rfcore.rfcore_ffsm_short_addr0.store_bits(addr & 0xff);
        self.rfcore.rfcore_ffsm_short_addr1.store_bits(addr >> 8);
    }

    /// Reads the current signal strength (RSSI) in dBm in the currently configured channel.
    pub fn get_rssi(&mut self) -> i32 {
        let mut rssi: i32 = 0;

        let was_off = if self
            .rfcore
            .rfcore_xreg_fsmstat0
            .fsm_ffctrl_state
            .read_bits()
            == 0
        {
            self.on();
            true
        } else {
            false
        };

        // Wait for a valid RSSI reading.
        loop {
            rssi = self.rfcore.rfcore_xreg_rssi.load_bits().try_into().unwrap();

            if rssi != RSSI_INVALID {
                break;
            }
        }
        rssi -= RSSI_OFFSET;

        if was_off {
            self.off();
        }

        rssi
    }

    /// Returns the current CCA threshold in dBm.
    pub fn get_cca_threshold(&self) -> i32 {
        let val: i32 = self
            .rfcore
            .rfcore_xreg_ccactrl0
            .cca_thr
            .read_bits()
            .try_into()
            .unwrap();

        val - RSSI_OFFSET
    }

    pub fn set_cca_threshold(&self, cca_threshold: i32) {
        self.rfcore
            .rfcore_xreg_ccactrl0
            .store_bits(((cca_threshold & 0xff) + RSSI_OFFSET).try_into().unwrap())
    }

    /// Returns the current TX power in dBm.
    pub fn get_tx_power(&self) -> i32 {
        let reg_val: u8 = (self.rfcore.rfcore_xreg_txpower.load_bits() & 0xff)
            .try_into()
            .unwrap();

        for (power, txpower_val) in OUTPUT_POWER.iter() {
            if reg_val >= *txpower_val {
                return *power;
            }
        }

        OUTPUT_POWER_MIN
    }

    /// Set the TX power to at least `tx_power` in dBm.
    /// This works with a lookup table. If the value of `tx_power` does not exist in the lookup
    /// table, TXPOWER will be set to the immediately higher available value.
    pub fn set_tx_power(&self, tx_power: i32) {
        // Lookup the power in the table.
        for (power, txpower_val) in OUTPUT_POWER.iter() {
            if tx_power <= *power {
                self.rfcore
                    .rfcore_xreg_txpower
                    .store_bits(*txpower_val as u32);
                return;
            }
        }
    }

    pub fn set_frame_filtering(&self, enable: bool) {
        if enable {
            self.rfcore.rfcore_xreg_frmfilt0.frame_filter_en.set_bit()
        } else {
            self.rfcore.rfcore_xreg_frmfilt0.frame_filter_en.clear_bit()
        }
    }

    pub fn set_shr_search(&self, enable: bool) {
        // XXX: check if this is done correctly
        if enable {
            self.rfcore.rfcore_xreg_frmctrl0.rx_mode.write_bits(0b00);
        } else {
            self.rfcore.rfcore_xreg_frmctrl0.rx_mode.write_bits(0x11);
        }
    }

    pub fn mac_timer_init(&self) {
        // TODO: mac timer is not yet implemented.
        todo!();
    }

    pub fn set_poll_mod(&mut self, enable: bool) {
        self.config.poll_mode = enable;

        if enable {
            self.mac_timer_init();

            // Mask out FIFOP
            self.rfcore
                .rfcore_xreg_rfirqm0
                .modify(|r| r.write_rfirqm(r.rfirqm() & !0x04));

            // Clear pending FIFOP
            self.rfcore.rfcore_sfr_rfirqf0.fifop.clear_bit();

        // TODO: disable RF in NVIC
        } else {
            self.rfcore
                .rfcore_xreg_rfirqm0
                .modify(|r| r.write_rfirqm(r.rfirqm() | 0x04));
            // TODO: enable RF in NVIC
        }
    }

    pub fn set_send_on_cca(&mut self, enable: bool) {
        self.config.send_on_cca = enable;
    }

    pub fn set_auto_ack(&self, enable: bool) {
        if enable {
            self.rfcore.rfcore_xreg_frmctrl0.autoack.set_bit();
        } else {
            self.rfcore.rfcore_xreg_frmctrl0.autoack.clear_bit();
        }
    }

    pub fn get_sfd_timestamp(&self) -> u32 {
        todo!();
    }

    pub fn set_test_mode(&mut self, enable: bool, modulated: bool) {
        let mut prev_frmctrl0: u32 = 0; // this needs to be static
        let mut prev_mdmtest1: u32 = 0; // this needs to be static
        let mut was_on: bool = false; // this needs to be static
        let mut mode = RADIO_POWER_MODE::OFF;

        // get_value(RADIO_PARAM_POWER_MODE, &mode);

        if enable {
            if mode == RADIO_POWER_MODE::CARRIER_ON {
                return;
            }

            was_on = mode == RADIO_POWER_MODE::ON;

            self.off();

            prev_frmctrl0 = self.rfcore.rfcore_xreg_frmctrl0.load_bits();

            self.rfcore.rfcore_xreg_frmctrl0.store_bits(0x42); // This constantly transmits random data.
            if !modulated {
                prev_mdmtest1 = self.rfcore.rfcore_xreg_mdmtest1.load_bits();
                self.rfcore.rfcore_xreg_mdmtest1.store_bits(0x18); // Adding this enables sending an unmodulated carrier.
            }

            self.rf_csp_istxon();
        } else {
            if mode != RADIO_POWER_MODE::CARRIER_ON {
                return;
            }

            self.rf_csp_isrfoff();

            self.rfcore.rfcore_xreg_frmctrl0.store_bits(prev_frmctrl0);

            if !modulated {
                self.rfcore.rfcore_xreg_mdmtest1.store_bits(prev_mdmtest1);
            }

            if was_on {
                self.on();
            }
        }
    }

    /// Perform a Clear-Channel Assessment (CCA) to find out if there is a packet in the air or
    /// not.
    pub fn channel_clear(&mut self) -> u32 {
        let was_off = if self
            .rfcore
            .rfcore_xreg_fsmstat0
            .fsm_ffctrl_state
            .read_bits()
            == 0
        {
            self.on();
            true
        } else {
            false
        };

        while !self.rfcore.rfcore_xreg_rssistat.rssi_valid.read_bit() {}

        let cca = if self.rfcore.rfcore_xreg_fsmstat1.cca.read_bit() {
            CC2538_RF_CCA_CLEAR
        } else {
            CC2538_RF_CCA_BUSY
        };

        if was_off {
            self.off();
        }

        cca
    }

    /// Turn on the radio.
    pub fn on(&mut self) {
        if !self.rf_status.rx_active {
            self.rf_csp_isflushrx();
            self.rf_csp_isrxon();
            self.rf_status.set_rx_active();
        }
    }

    /// Turn of the radio.
    pub fn off(&mut self) {
        // Wait for ongoing TX to complete.

        while self.rfcore.rfcore_xreg_fsmstat1.tx_active.read_bit() {}

        if !self.rfcore.rfcore_xreg_fsmstat1.fifop.read_bit() {
            self.rf_csp_isflushrx();
        }

        if self.rfcore.rfcore_xreg_rxenable.load_bits() != 0 {
            self.rf_csp_isrfoff();
        }

        self.rf_status.clear_rx_active();
    }

    pub fn init(&self) {
        todo!();
    }

    /// Prepare the radio with a packet to be sent.
    pub fn prepare(&mut self, payload: &[u8]) {
        if payload.len() > MAX_PAYLOAD_LEN as usize {
            return; // XXX: should return an error.
        }

        while self.rfcore.rfcore_xreg_fsmstat1.tx_active.read_bit() {}

        if !self.rf_status.rx_active {
            self.on();
        }

        self.rf_csp_isflushtx();

        self.rfcore
            .rfcore_sfr_rfdata
            .store_bits(payload.len() as u32 + CHECKSUM_LEN);

        if CC2538_RF_CONF_TX_USE_DMA {
            // Set the transfer source's end andress.
            self.dma_driver
                .set_channel_src(
                    CC2538_RF_CONF_TX_DMA_CHAN,
                    payload.as_ptr() as u32 + payload.len() as u32 - 1,
                )
                .unwrap();

            // Configure the control word.
            self.dma_driver
                .set_channel_control_word(
                    CC2538_RF_CONF_TX_DMA_CHAN,
                    DMA_TX_FLAGS | DmaDriver::transfer_size(payload.len()) as u32,
                )
                .unwrap();

            // Enable the RF TX uDMA channel.
            self.dma_driver
                .channel_enable(CC2538_RF_CONF_TX_DMA_CHAN)
                .unwrap();

            // Trigger the uDMA transfer.
            self.dma_driver
                .channel_sw_request(CC2538_RF_CONF_TX_DMA_CHAN)
                .unwrap();
        } else {
            // Add if structure for when uDMA is used.
            for b in payload.iter() {
                self.rfcore.rfcore_sfr_rfdata.store_bits((*b).into());
            }
        }
    }

    /// Send the packet that has previously been prepared.
    pub fn transmit(&mut self, len: usize) {
        let mut was_off = false;
        if len > MAX_PAYLOAD_LEN as usize {
            return; // XXX: should return an error
        }

        if !self.rf_status.rx_active {
            self.on();
            was_off = true;
            // wait some time to wake up
        }

        if self.config.send_on_cca {
            if self.channel_clear() == CC2538_RF_CCA_BUSY {
                return;
            }
        }

        if self.rfcore.rfcore_xreg_fsmstat1.sfd.read_bit() {
            return;
        }

        self.rf_csp_istxon();
        let mut counter = 0;

        while !self.rfcore.rfcore_xreg_fsmstat1.tx_active.read_bit() && counter < 3 {
            counter += 1;
            // delay for 6 µsec
        }

        if !self.rfcore.rfcore_xreg_fsmstat1.tx_active.read_bit() {
            // tx is never active
            self.rf_csp_isflushtx();
            return;
        } else {
            while self.rfcore.rfcore_xreg_fsmstat1.tx_active.read_bit() {}
        }

        if was_off {
            self.off();
        }

        // Ok(())
    }

    /// Prepare and transmit a packet.
    pub fn send(&mut self, payload: &[u8]) {
        self.prepare(payload);
        self.transmit(payload.len());
    }

    /// Read a received packet into a buffer.
    pub fn read(&self, buffer: &mut [u8]) {
        if self.rfcore.rfcore_xreg_fsmstat1.fifop.read_bit() {
            return;
        }

        let mut len = self.rfcore.rfcore_sfr_rfdata.load_bits();

        if len > CC2538_RF_MAX_PACKET_LEN {
            // something went wrong.
            self.rf_csp_isflushrx();
            return; // XXX: should return error
        }

        if len <= CC2538_RF_MIN_PACKET_LEN {
            // the received message is too short
            self.rf_csp_isflushrx();
            return; // XXX: should return error
        }

        if len - CHECKSUM_LEN > buffer.len() as u32 {
            // the received message is too long
            self.rf_csp_isflushrx();
            return; // XXX: should return error
        }

        len -= CHECKSUM_LEN;

        for i in 0..len {
            buffer[i as usize] = self.rfcore.rfcore_sfr_rfdata.load_bits() as u8;
        }

        let rssi = self.rfcore.rfcore_sfr_rfdata.load_bits() as u8 - RSSI_OFFSET as u8; // this should be static
        let crc_corr = self.rfcore.rfcore_sfr_rfdata.load_bits() as u8; // this should be static

        if crc_corr & CRC_BIT_MASK == 1 {
            // packetbuf_set_attr(PACKETBUF_ATTR_RSSI, rssi);
            // packetbuf_set_attr(PACKETBUF_ATTR_LINK_QUALITY, crc_corr & LQI_BIT_MASK);
        } else {
            self.rf_csp_isflushrx();
            return;
        }

        if !self.config.poll_mode {
            if self.rfcore.rfcore_xreg_fsmstat1.fifop.read_bit() {
                if self.rfcore.rfcore_xreg_fsmstat1.fifo.read_bit() {
                    // process_poll(&cc2538_rf_process);
                } else {
                    self.rf_csp_isflushrx();
                }
            }
        }
    }

    /// Check if the radio driver is currently receiving a packet.
    pub fn receiving_packet(&self) -> bool {
        // SFD is high when transmitting and receiving.
        // TX_ACTIVE is only high when transmittering.
        // Thus TX_ACTIVE must be low to know if we are receiving.
        self.rfcore.rfcore_xreg_fsmstat1.sfd.read_bit()
            & self.rfcore.rfcore_xreg_fsmstat1.tx_active.read_bit()
    }

    /// Check if the radio driver has just received a packet.
    pub fn pending_packet(&self) -> bool {
        self.rfcore.rfcore_xreg_fsmstat1.fifop.read_bit()
    }

    /// Get a radio parameter value.
    pub fn get_value(&self) {
        todo!();
    }

    /// Set a radio parameter value.
    pub fn set_value(&self) {
        todo!();
    }

    /// Get a radio parameter object.
    pub fn get_object(&self) {
        todo!();
    }

    /// Set a radio parameter object.
    pub fn set_object(&self) {
        todo!();
    }

    /// Send an RX ON command strobe to the CSP.
    fn rf_csp_isrxon(&self) {
        self.rfcore
            .rfcore_sfr_rfst
            .store_bits(RfCspOp::ISRXON as u32);
    }

    /// Send a TX ON command strobe to the CSP.
    fn rf_csp_istxon(&self) {
        self.rfcore
            .rfcore_sfr_rfst
            .store_bits(RfCspOp::ISTXON as u32);
    }

    /// Send a RF OFF command strobe to the CSP.
    fn rf_csp_isrfoff(&self) {
        self.rfcore
            .rfcore_sfr_rfst
            .store_bits(RfCspOp::ISRFOFF as u32);
    }

    /// Flush the RX FIFO.
    fn rf_csp_isflushrx(&self) {
        self.rfcore
            .rfcore_sfr_rfst
            .store_bits(RfCspOp::ISFLUSHRX as u32);
    }

    /// Flush the TX FIFO.
    fn rf_csp_isflushtx(&self) {
        self.rfcore
            .rfcore_sfr_rfst
            .store_bits(RfCspOp::ISFLUSHTX as u32);
    }

    /// Implementation of the CC2538 RF driver process.
    ///
    /// This process should be started by init(). It simply sits there waiting for an event. Upon
    /// frame reception, the RX ISR will poll this process. Subsequently, the contiki core will
    /// generate an event which will call this process so that the received frame can be picked up
    /// from the RF RX FIFO.
    pub fn rf_process(&self) {
        todo!();
    }

    /// This is the interrupt service routine for all RF interrupts relating to RX and TX. Error
    /// conditions are handled by cc2538_rf_err_isr(). Currently, we only acknowledge the FIFOP
    /// interrupt source.
    pub fn rf_rx_tx_isr(&self) {
        todo!();
        if !self.config.poll_mode {
            // process_poll(&process);
        }

        self.rfcore.rfcore_sfr_rfirqf0.store_bits(0);
    }

    pub fn rf_err_isr(&self) {
        // TODO: Signal when to reset.

        todo!();

        self.rfcore.rfcore_sfr_rferrf.store_bits(0);
    }
}
