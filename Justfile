cortexm_core := 'cortexm4f_r0p1'
tisl_mcu := 'cc2538'
export DRONE_RUSTFLAGS := '--cfg cortexm_core="' + cortexm_core + '" ' + '--cfg tisl_mcu="' + tisl_mcu + '"'
target := 'thumbv7em-none-eabihf'
features := ''

# Install dependencies
deps:
	rustup target add {{target}}
	rustup component add clippy
	rustup component add rustfmt
	type cargo-readme >/dev/null || cargo +stable install cargo-readme

# Reformat the source code
fmt:
	cargo fmt

# Check the source code for mistakes
lint:
	cargo clippy --package drone-tisl-map-svd
	drone env {{target}} -- cargo clippy --features "{{features}}" --all --exclude drone-tisl-map-svd

# Build the documentation
doc:
	cargo doc --package drone-tisl-map-svd
	drone env {{target}} -- cargo doc --features "{{features}}" --package drone-tisl-map

# Open the documentation in a browser
doc-open: doc
	drone env {{target}} -- cargo doc --features "{{features}}" --package drone-tisl-map --open

# Run the tests
test:
	drone env -- cargo test --features "{{features}} std" --package drone-tisl-map

check:
    drone env {{target}} -- cargo check --features "{{features}}"
